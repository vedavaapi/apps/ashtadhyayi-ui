import { writable, derived } from 'svelte/store';
export const apiData = writable([]);
export const derivedData = writable([]);
export const combineData = writable(['...']);
export const combineList = derived(combineData, ($combineData) => {
    if ($combineData){
        let result = []
        $combineData.forEach(item=>{
           // console.log('item',item)
            result.push(item.dhatu_label+'+'+item.pratyaya);
        });
       // console.log('result',result)
        return result;
    }
})
export const derivedList = derived(derivedData, ($derivedData) => {
   // console.log('came to set')
    if ($derivedData){
        let result = []
        $derivedData.forEach(item=>{
            result.push(item.pada);
        });
       // console.log('result',result)
        return result.join('+');
    }
})
export const KrdantaList = derived(apiData, ($apiData) => {
    if ($apiData){
        // $apiData = $apiData.map(item => {
        //                 let text = item.output.split(':')[1].split('-');
        //                 item.sutra_text = item.sutra_text.replace(/[^\w\s]/gi, '');
        //                 console.log('text',text)
        //                 if(text) {
        //                     item.text1 = text[0].replace(/[^\w\s]/gi, '');
        //                     item.text2 = text[1].replace(/[^\w\s]/gi, '');
        //                     item.text3 = text[2].replace(/[^\w\s]/gi, '');
        //                 }
                        
        //                 return item
        //             });
        let result = [];
        let count = 0;
        $apiData.forEach(item=>{
//            let text = item.output.split(':')[1].split('-');
            let text = item.output.split('-');
//            item.sutra_text = item.sutra_text.replace(/[^\w\s]/gi, '');
            item.out_padas = text[0].replace(/[`!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            item.out_form = text[1].replace(/[`!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            item.trace = [].concat(["mahavakya: "+ item.sutra_mahavakya], 
                    item.trace);
           // result.push(item);
           //if(count==0){
           // let item1 = JSON.parse(JSON.stringify(item));
           // let text1 = item.input.split('-');
           // item1.out_padas = text1[0].replace(/[`!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
           // item1.out_form = text1[1].replace(/[`!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
           // console.log('input',item,item1);
           // result.push(item1,item);
           //}
           count++;
           item.seqno = count;
           result.push(item);
           console.log('result from KrdantaList derived',result);
        });
        return result;        
    }
    return [];
  });
