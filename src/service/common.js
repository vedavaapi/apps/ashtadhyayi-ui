const getdhaatuLables = function(labelName, outputSelected, category=null){ 
    let queryParam = {};
    let queryURL = "";
    let result = [];

    if(labelName!=null && labelName!=''){
        queryParam.show = labelName
    }
    queryParam.outscript = outputSelected
    queryParam.inscript = outputSelected
    if(category!=null && category!=''){
        queryParam.category = category
    }
    //console.log(queryParam, category)
    const params = new URLSearchParams(queryParam);
      //?show="+labelName+"&outscript="+outputSelected
    if(labelName == "dhatu_labels" || category != null) {
        queryURL = "https://paias-api.sambhasha.ksu.ac.in/info/dhatus?"+params.toString()
    }
    else {
        queryURL = "https://paias-api.sambhasha.ksu.ac.in/info/krdantas?"+params.toString()
    }
    return fetch(queryURL)
    //return fetch("https://paias-api.sambhasha.ksu.ac.in/info/dhatus?"+params.toString())
        .then(response => response.json())
        .then(data => {
                /*if (labelName == "dhatu_labels") {
                for (let obj of data.result) {
                    result.push(obj["dhatu_label"]);
                }
                
                return result;
                

                }*/
                
                    return data.result;
                
                // console.log(data);
        }).catch(error => {
            console.log(error);
            return [];
        });
}

const fetchList = function(dhaatuLabel, pratyayaLabel, inscript, outscript) {
    return fetch("https://paias-api.sambhasha.ksu.ac.in/krtgen?dhatu_label="+dhaatuLabel+"&pratyaya="+pratyayaLabel+"&inscript="+inscript+"&outscript="+outscript+"&logging_level=0")
            .then(response => response.json())
            .then(data => {
                    // console.log(data);
                        return data
            }).catch(error => {
                console.log(error);
                return [];
            });
}

const fetchKradant = function(krdantaLabel, inscript, outscript) {
    return fetch("https://paias-api.sambhasha.ksu.ac.in/derive?krdanta="+krdantaLabel+"&inscript="+inscript+"&outscript="+outscript+"&logging_level=0",{headers: {'Accept-Language': '*'}})
            .then(response => response.json())
            .then(data => {
                    // console.log(data);
                        return data
            }).catch(error => {
                console.log(error);
                return [];
            });
}
exports.getdhaatuLables =  getdhaatuLables;
exports.fetchList =  fetchList;
exports.fetchKradant = fetchKradant;
